﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace addressBookAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Number
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long NumberId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey("Person")]
        public long PersonId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey("Address")]
        public long AddressId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [MaxLength(15)]
        public string NumberType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [MaxLength(15)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public bool DeleteFlag { get; set; }

    }
}
