﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using addressBookAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace addressBook.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private readonly AddressBookContext PersonContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContext"></param>
        public PersonsController(AddressBookContext pContext) => PersonContext = pContext;
        /// <summary>Gets all persons</summary>
        // GET api/getPersons
        [HttpGet]
        public ActionResult GetAllPersons()
        {
            try
            {
                return Ok(PersonContext.Persons.OrderBy(a => a.PersonId).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Get person by ID</summary>
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> GetPersonById(long id)
        {
            try
            {
                return Ok(PersonContext.Persons.Find(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Add person</summary>
        // POST api/values
        [HttpPost]
        public ActionResult Add([FromBody] Person person)
        {
            try
            {
                PersonContext.Persons.Add(person);
                PersonContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : ControllerBase
    {
        private readonly AddressBookContext AddressContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContext"></param>
        public AddressesController(AddressBookContext pContext) => AddressContext = pContext;
        /// <summary>Gets all addresses</summary>
        // GET api/getAddresses
        [HttpGet]
        public ActionResult GetAllPersons()
        {
            try
            {
                return Ok(AddressContext.Persons.OrderBy(a => a.PersonId).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Get address by ID</summary>
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> GetPersonById(long id)
        {
            try
            {
                return Ok(AddressContext.Persons.Find(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Add address</summary>
        // POST api/values
        [HttpPost]
        public ActionResult Add([FromBody] Person person)
        {
            try
            {
                AddressContext.Persons.Add(person);
                AddressContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
    [Route("api/[controller]")]
    [ApiController]
    public class NumbersController : ControllerBase
    {
        private readonly AddressBookContext NumberContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContext"></param>
        public NumbersController(AddressBookContext pContext) => NumberContext = pContext;
        /// <summary>Gets all numbers</summary>
        // GET api/getAddresses
        [HttpGet]
        public ActionResult GetAllPersons()
        {
            try
            {
                return Ok(NumberContext.Persons.OrderBy(a => a.PersonId).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Get number by ID</summary>
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> GetPersonById(long id)
        {
            try
            {
                return Ok(NumberContext.Persons.Find(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Add number</summary>
        // POST api/values
        [HttpPost]
        public ActionResult Add([FromBody] Person person)
        {
            try
            {
                NumberContext.Persons.Add(person);
                NumberContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
